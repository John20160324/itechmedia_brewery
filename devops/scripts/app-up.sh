#!/bin/bash

GREEN="\033[0;32m"
RESET="\033[0m"

APP_NETWORK='brewery-net'
FOUND_APP_NETWORK=$(docker network ls --format="{{.Name}}" -f name=${APP_NETWORK} | grep ${APP_NETWORK})

if [[ -z ${FOUND_APP_NETWORK} ]]
then
    docker network create ${APP_NETWORK}
fi

WINPTY=''
if [[ -n ${WINDIR} ]]
then
   echo "WINDIR is defined"
   WINPTY='winpty '
fi

# start application
NAME_PREFIX="$1"
DEVOPS_DIR="$2"

# ensure that old containers are removed
echo -e "\n${GREEN}Stopping containers${RESET}"
docker-compose --project-directory ${DEVOPS_DIR} -f ${DEVOPS_DIR}/docker-compose.yml -p $NAME_PREFIX stop

echo -e "\n${GREEN}Removing containers${RESET}"
docker-compose --project-directory ${DEVOPS_DIR} -f ${DEVOPS_DIR}/docker-compose.yml -p $NAME_PREFIX rm -f

## start application
echo -e "\n${GREEN}Building images${RESET}"
docker-compose --project-directory ${DEVOPS_DIR} -f ${DEVOPS_DIR}/docker-compose.yml -p $NAME_PREFIX build --pull

echo -e "\n${GREEN}Up containers${RESET}"
docker-compose --project-directory ${DEVOPS_DIR} -f ${DEVOPS_DIR}/docker-compose.yml -p $NAME_PREFIX up -d --force-recreate

echo -e "\n${GREEN}Installing dependencies${RESET}"
$WINPTY docker-compose --project-directory ${DEVOPS_DIR} -f ${DEVOPS_DIR}/docker-compose.yml -p $NAME_PREFIX  \
  exec php sh -c "composer install -a -n --prefer-dist;"
$WINPTY docker-compose --project-directory ${DEVOPS_DIR} -f ${DEVOPS_DIR}/docker-compose.yml -p $NAME_PREFIX  \
  exec php sh -c "composer app:up-db;"
