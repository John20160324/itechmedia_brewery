<?php

namespace App\Test\TestCase\Service\APIClient\Openbrewery;

use App\DTO\Openbrewery\Item;
use App\Service\APIClient\Openbrewery;
use App\Service\Exception\ApiClientException;
use DG\BypassFinals;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class FetchJsonDataTest extends TestCase
{
    private const BASE_URL = 'https://ololo.com';
    private const PER_PAGE = 10;
    private const RESPONSE_CONTENT_AS_ARRAY = [
        [
            "id" => 2,
            "name" => "Avondale Brewing Co",
            "brewery_type" => "micro",
            "street" => "201 41st St S",
            "city" => "Birmingham",
            "state" => "Alabama",
            "postal_code" => "35222-1932",
            "country" => "United States",
            "longitude" => "-86.774322",
            "latitude" => "33.524521",
            "phone" => "2057775456",
            "website_url" => "http=>//www.avondalebrewing.com",
            "updated_at" => "2018-08-23T23:00:00.825Z",
            "tag_list" => [],
        ],
        [
            "id" => 44,
            "name" => "Trim Tab Brewing",
            "brewery_type" => "micro",
            "street" => "2721 5th Ave S",
            "city" => "Birmingham",
            "state" => "Alabama",
            "postal_code" => "35233-3401",
            "country" => "United States",
            "longitude" => "-86.7914000624146",
            "latitude" => "33.5128492349817",
            "phone" => "2057030536",
            "website_url" => "http=>//www.trimtabbrewing.com",
            "updated_at" => "2018-08-23T23:20:31.423Z",
            "tag_list" => [],
        ],
        [
            "id" => 46,
            "name" => "Yellowhammer Brewery",
            "brewery_type" => "micro",
            "street" => "2600 Clinton Ave W",
            "city" => "Huntsville",
            "state" => "Alabama",
            "postal_code" => "35805-3046",
            "country" => "United States",
            "longitude" => "-86.5932014",
            "latitude" => "34.7277523",
            "phone" => "2569755950",
            "website_url" => "http=>//www.yellowhammerbrewery.com",
            "updated_at" => "2018-08-23T23:20:33.102Z",
            "tag_list" => [],
        ],
        [
            "id" => 55,
            "name" => "Bearpaw River Brewing Co",
            "brewery_type" => "micro",
            "street" => "4605 E Palmer Wasilla Hwy",
            "city" => "Wasilla",
            "state" => "Alaska",
            "postal_code" => "99654-7679",
            "country" => "United States",
            "longitude" => "-149.4127103",
            "latitude" => "61.5752695",
            "phone" => "",
            "website_url" => "http=>//bearpawriverbrewing.com",
            "updated_at" => "2018-08-23T23:20:40.743Z",
            "tag_list" => [],
        ],
        [
            "id" => 76,
            "name" => "King Street Brewing Co",
            "brewery_type" => "micro",
            "street" => "9050 King Street",
            "city" => "Anchorage",
            "state" => "Alaska",
            "postal_code" => "99515",
            "country" => "United States",
            "longitude" => "-149.879076042937",
            "latitude" => "61.1384893547315",
            "phone" => "",
            "website_url" => "",
            "updated_at" => "2018-08-23T23:20:57.179Z",
            "tag_list" => [],
        ],
    ];

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        BypassFinals::enable();
    }

    public function testMain(): void
    {
        $response = $this->createMock(ResponseInterface::class);
        $response
            ->expects(self::once())
            ->method('toArray')
            ->willReturn(self::RESPONSE_CONTENT_AS_ARRAY);

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient
            ->expects(self::once())
            ->method('request')
            ->with(...['GET', self::BASE_URL, ['query' => ['per_page' => self::PER_PAGE]]])
            ->willReturn($response);

        $logger = $this->createMock(LoggerInterface::class);
        $logger
            ->expects(self::never())
            ->method('error');

        $result = (new Openbrewery(self::BASE_URL, self::PER_PAGE, $httpClient, $logger))->fetchJsonData();

        $this->assertEquals($this->hydrateContent(self::RESPONSE_CONTENT_AS_ARRAY), $result);
    }

    public function testThrowExpectedException(): void
    {
        $this->expectException(ApiClientException::class);
        $this->expectExceptionMessage('Oops! Can\'t sync data');

        $statusCode = 400;
        $responseError = 'response error';

        $expectedException = $this->createMock(ServerException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response
            ->expects(self::once())
            ->method('getInfo')
            ->willReturn(['error' => $responseError]);
        $response
            ->expects(self::once())
            ->method('getStatusCode')
            ->willReturn($statusCode);
        $response
            ->expects(self::once())
            ->method('toArray')
            ->willThrowException($expectedException);

        $expectedException
            ->expects(self::once())
            ->method('getResponse')
            ->willReturn($response);

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient
            ->expects(self::once())
            ->method('request')
            ->willReturn($response);

        $logger = $this->createMock(LoggerInterface::class);
        $logger
            ->expects(self::once())
            ->method('error')
            ->with(...[$responseError, ['status code' => $statusCode]]);

        (new Openbrewery(self::BASE_URL, self::PER_PAGE, $httpClient, $logger))->fetchJsonData();
    }

    private function hydrateContent(array $data): array
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = Item::fromFetchData($item);
        }

        return $result;
    }
}
