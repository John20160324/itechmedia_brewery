<?php

namespace App\Test\TestCase\Service\SyncData\DataProvider\OpenDb\Openbrewery;

use App\Entity\Brewery;
use App\Repository\BreweryRepository;
use App\Service\APIClient\Openbrewery as APIClient;
use App\Service\SyncData\DataProvider\OpenDb\Openbrewery;
use App\Service\SyncData\DataProvider\SyncLocker;
use App\ValueObject\SyncStatus;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Lock\LockInterface;

class GetSyncStatusTest extends TestCase
{

    public function testWithAcquired(): void
    {
        $isAcquired = true;

        $apiClient = $this->createMock(APIClient::class);
        $em = $this->createMock(EntityManagerInterface::class);

        $lock = $this->createMock(LockInterface::class);
        $lock
            ->expects($this->once())
            ->method('isAcquired')
            ->willReturn($isAcquired);

        $syncLocker = $this->createMock(SyncLocker::class);
        $syncLocker
            ->expects($this->once())
            ->method('getLock')
            ->willReturn($lock);

        $result = (new Openbrewery($em, $syncLocker, $apiClient))->getSyncStatus();

        $this->assertEquals(SyncStatus::inProgress(), $result);
    }

    /**
     * @dataProvider dataProviderForCasesWithoutAcquired
     *
     * @param bool       $isAcquired
     * @param bool       $hasData
     * @param SyncStatus $expectedResult
     */
    public function testWithoutAcquired(bool $isAcquired, bool $hasData, SyncStatus $expectedResult): void
    {
        $apiClient = $this->createMock(APIClient::class);

        $repository = $this->createMock(BreweryRepository::class);
        $repository
            ->expects($this->once())
            ->method('hasData')
            ->willReturn($hasData);

        $em = $this->createMock(EntityManagerInterface::class);
        $em
            ->expects($this->once())
            ->method('getRepository')
            ->with(...[Brewery::class])
            ->willReturn($repository);

        $lock = $this->createMock(LockInterface::class);
        $lock
            ->expects($this->once())
            ->method('isAcquired')
            ->willReturn($isAcquired);

        $syncLocker = $this->createMock(SyncLocker::class);
        $syncLocker
            ->expects($this->once())
            ->method('getLock')
            ->willReturn($lock);

        $result = (new Openbrewery($em, $syncLocker, $apiClient))->getSyncStatus();

        $this->assertEquals($expectedResult, $result);
    }

    public function dataProviderForCasesWithoutAcquired()
    {
        yield 'case with empty DB' => [
            'isAcquired' => false,
            'hasData' => false,
            'result' => SyncStatus::never(),
        ];

        yield 'case with fill DB' => [
            'isAcquired' => false,
            'hasData' => true,
            'result' => SyncStatus::done(),
        ];
    }
}
