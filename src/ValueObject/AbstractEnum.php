<?php

namespace App\ValueObject;

abstract class AbstractEnum
{
    protected $value;

    public function __construct($value)
    {
        if ($value instanceof self) {
            $value = $value->getValue();
        }

        if (!in_array($value, static::getAvailableValues(), true)) {
            //TODO: more readable with an allowed values list
            throw new ValueObjectOutOfBoundsException(sprintf('Invalid value "%d".', $value));
        }

        $this->value = $value;
    }

    /**
     * @return static
     */
    public static function fromValue($value): self
    {
        return new static($value);
    }

    public function getValue()
    {
        return $this->value;
    }

    public function equals(self $value): bool
    {
        return $this->getValue() === $value->getValue();
    }

    public function __toString()
    {
        return $this->value;
    }

    abstract public static function getAvailableValues(): array;
}
