<?php

namespace App\ValueObject;

class ValueObjectOutOfBoundsException extends \OutOfBoundsException
{
}