<?php

namespace App\ValueObject;

final class SyncStatus extends AbstractEnum
{
    public const NEVER = 'never';
    public const IN_PROGRESS = 'in_progress';
    public const DONE = 'done';

    public static function never(): self
    {
        return new self(self::NEVER);
    }

    public static function inProgress(): self
    {
        return new self(self::IN_PROGRESS);
    }

    public static function done(): self
    {
        return new self(self::DONE);
    }

    public static function getAvailableValues(): array
    {
        return [
            self::NEVER,
            self::IN_PROGRESS,
            self::DONE,
        ];
    }
}
