<?php

namespace App\ValueObject;

final class FilterField extends AbstractEnum
{
    public const NAME = 'name';
    public const STREET = 'street';
    public const CITY = 'city';
    public const STATE = 'state';

    public static function name(): self
    {
        return new self(self::NAME);
    }

    public static function street(): self
    {
        return new self(self::STREET);
    }

    public static function city(): self
    {
        return new self(self::CITY);
    }

    public static function state(): self
    {
        return new self(self::STATE);
    }

    public static function getAvailableValues(): array
    {
        return [
            self::NAME,
            self::STREET,
            self::CITY,
            self::STATE,
        ];
    }
}
