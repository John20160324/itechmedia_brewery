<?php

namespace App\DTO\Openbrewery;

class Item
{
    private int $id;

    private string $name;

    private string $brewery_type;

    private string $street;

    private string $city;

    private string $state;

    private string $postal_code;

    private string $country;

    private string $longitude;

    private string $latitude;

    private ?string $phone;

    private ?string $website_url;

    private \DateTimeImmutable $updated_at;

    /**
     * @var string[]
     */
    private array $tag_list;

    private function __construct()
    {
    }

    public static function fromFetchData(array $data): self
    {
        $instance = new self();
        $instance->id = $data['id'];

        $instance->name = $data['name'];
        $instance->brewery_type = $data['brewery_type'];
        $instance->street = $data['street'];
        $instance->city = $data['city'];
        $instance->state = $data['state'];
        $instance->postal_code = $data['postal_code'];
        $instance->country = $data['country'];

        $instance->longitude = $data['longitude'];
        $instance->latitude = $data['latitude'];

        $instance->phone = $data['phone'] ?? null;
        $instance->website_url = $data['website_url'] ?? null;

        $instance->updated_at = new \DateTimeImmutable($data['updated_at']);
        $instance->tag_list = $data['tag_list']??[];

        return $instance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBreweryType(): string
    {
        return $this->brewery_type;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getPostalCode(): string
    {
        return $this->postal_code;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getWebsiteUrl(): ?string
    {
        return $this->website_url;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updated_at;
    }

    /**
     * @return string[]
     */
    public function getTagList(): array
    {
        return $this->tag_list;
    }
}
