<?php

namespace App\Service\SyncData;

use App\Service\SyncData\DataProvider\OpenDb\Openbrewery;
use App\Service\SyncData\DataProvider\ProviderInterface;
use App\ValueObject\SyncStatus;

class DataSynchronizer
{
    protected const SYNC_LOCK = 'Openbrewery_lock';

    /**
     * @var ProviderInterface|Openbrewery
     */
    private ProviderInterface $dataProvider;

    public function __construct(ProviderInterface $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    public function fetchData(SyncStatus $syncStatus): SyncStatus
    {
        if ($syncStatus->equals(SyncStatus::never())) {
            $this->dataProvider->sync();

            return SyncStatus::done();
        } else {
            return $syncStatus;
        }
    }

    public function getSyncStatus(): SyncStatus
    {
        return $this->dataProvider->getSyncStatus();
    }
}
