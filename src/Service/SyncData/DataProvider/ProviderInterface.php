<?php


namespace App\Service\SyncData\DataProvider;

use App\ValueObject\SyncStatus;

interface ProviderInterface
{
    public function getSyncStatus(): SyncStatus;

    public function sync(): void;
}