<?php

namespace App\Service\SyncData\DataProvider;

use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\LockInterface;

class SyncLocker
{
    private const SYNC_LOCK = 'concurrency_sync_locker';

    /**
     * @var LockInterface
     */
    private LockInterface $lock;

    public function __construct(LockFactory $lockFactory)
    {
        $this->lock = $lockFactory->createLock(self::SYNC_LOCK);
    }

    public function getLock(): LockInterface
    {
        return $this->lock;
    }
}