<?php

namespace App\Service\SyncData\DataProvider\OpenDb;

use App\DTO\Openbrewery\Item;
use App\Entity\Brewery;
use App\Service\APIClient\Openbrewery as APIClient;
use App\Service\SyncData\DataProvider\ProviderInterface;
use App\Service\SyncData\DataProvider\SyncLocker;
use App\ValueObject\SyncStatus;
use Doctrine\ORM\EntityManagerInterface;

class Openbrewery implements ProviderInterface
{
    private const NUMBER_ROWS_IN_TRANSACTION = 100;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var SyncLocker
     */
    private SyncLocker $syncLocker;

    /**
     * @var APIClient
     */
    private APIClient $apiClient;

    public function __construct(EntityManagerInterface $entityManager, SyncLocker $syncLocker, APIClient $apiClient)
    {
        $this->entityManager = $entityManager;
        $this->syncLocker = $syncLocker;
        $this->apiClient = $apiClient;
    }

    public function getSyncStatus(): SyncStatus
    {
        if ($this->syncLocker->getLock()->isAcquired()) {
            $status = SyncStatus::inProgress();
        } else {
            $repository = $this->entityManager->getRepository(Brewery::class);
            $status = $repository->hasData()
                ? SyncStatus::done()
                : SyncStatus::never();
        }

        return $status;
    }

    public function sync(): void
    {
        $this->syncLocker->getLock()->acquire();

        $resultItems = $this->apiClient->fetchJsonData();

        $this->storeData(...$resultItems);

        $this->syncLocker->getLock()->release();
    }

    private function storeData(Item ...$items)
    {
        $splices = array_chunk($items, self::NUMBER_ROWS_IN_TRANSACTION);

        foreach ($splices as $slice) {
            $this->entityManager->beginTransaction();
            /** @var Item $item */
            foreach ($slice as $item) {
                $entity = (new Brewery())
                    ->setBreweryType($item->getBreweryType())
                    ->setCity($item->getCity())
                    ->setCountry($item->getCountry())
                    ->setExternalId($item->getId())
                    ->setLatitude($item->getLatitude())
                    ->setLongitude($item->getLongitude())
                    ->setName($item->getName())
                    ->setPhone($item->getPhone())
                    ->setPostalCode($item->getPostalCode())
                    ->setState($item->getState())
                    ->setStreet($item->getStreet())
                    ->setTagList($item->getTagList())
                    ->setUpdatedAt($item->getUpdatedAt())
                    ->setWebsiteUrl($item->getWebsiteUrl());

                $this->entityManager->persist($entity);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        }
    }
}