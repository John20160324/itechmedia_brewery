<?php

namespace App\Service\Exception;

class SyncLockException extends \RuntimeException implements SyncDataExceptionInterface
{

}