<?php

namespace App\Service\Search;

use App\Entity\Brewery;
use App\Repository\BreweryRepository;
use App\ValueObject\FilterField;

class SearchBrewery
{
    /**
     * @var BreweryRepository
     */
    private BreweryRepository $repository;

    public function __construct(BreweryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FilterField $filterField
     * @param string      $value
     *
     * @return Brewery[]
     */
    public function search(FilterField $filterField, string $value): array
    {
        return $this->repository->findByFiledAndValue($filterField, $value);
    }
}