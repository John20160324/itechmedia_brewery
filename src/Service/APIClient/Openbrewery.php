<?php

namespace App\Service\APIClient;

use App\DTO\Openbrewery\Item;
use App\Service\Exception\ApiClientException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Openbrewery
{
    private string $baseUrl;

    private int $perPage;

    private HttpClientInterface $httpClient;

    private LoggerInterface $logger;

    public function __construct(string $baseUrl, int $perPage, HttpClientInterface $httpClient, LoggerInterface $logger)
    {
        $this->baseUrl = $baseUrl;
        $this->perPage = $perPage;
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    /**
     * @return Item[]
     */
    public function fetchJsonData(): array
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                $this->baseUrl,
                [
                    'query' => [
                        'per_page' => $this->perPage,
                    ],
                ]
            );

            $data = $response->toArray();

        } catch (HttpExceptionInterface $httpException) {
            $response = $httpException->getResponse();
            $this->logger->error($response->getInfo()['error'], ['status code' => $response->getStatusCode()]);
            throw new ApiClientException('Oops! Can\'t sync data');
        }

        return $this->hydrateContent($data);
    }

    /**
     * @param array $data
     *
     * @return Item[]
     */
    private function hydrateContent(array $data): array
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = Item::fromFetchData($item);
        }

        return $result;
    }
}