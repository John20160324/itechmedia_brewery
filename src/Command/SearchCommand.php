<?php

namespace App\Command;

use App\Service\Exception\SyncDataExceptionInterface;
use App\Service\Exception\SyncLockException;
use App\Service\Search\SearchBrewery;
use App\Service\SyncData\DataSynchronizer;
use App\ValueObject\FilterField;
use App\ValueObject\SyncStatus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class SearchCommand extends Command
{
    protected static $defaultName = 'app:search';

    /**
     * @var DataSynchronizer
     */
    private DataSynchronizer $dataSynchronizer;

    /**
     * @var SearchBrewery
     */
    private SearchBrewery $searchBrewery;

    protected function configure()
    {
        $this
            ->setDescription('Search the best a brewery.')
            ->setHelp(
                sprintf(
                    'This command has filter by fields \'%s\'.',
                    implode('\', \'', FilterField::getAvailableValues())
                )
            );
    }

    public function __construct(string $name = null, DataSynchronizer $dataSynchronizer, SearchBrewery $searchBrewery)
    {
        parent::__construct($name);

        $this->dataSynchronizer = $dataSynchronizer;
        $this->searchBrewery = $searchBrewery;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Welcome to line-search by the best breweries!');

        try {
            $this->prepareData($io);
        } catch (SyncDataExceptionInterface $exception) {
            $io->error($exception->getMessage());

            return 0;
        }

        $io->title('Let\'s find something...');

        $this->askWhile($io);

        return 0;
    }

    private function prepareData(SymfonyStyle $io): void
    {
        $prevSyncStatus = $this->dataSynchronizer->getSyncStatus();

        $this->showDescriptionBySyncStatus($io, $prevSyncStatus);

        $newSyncStatus = $this->dataSynchronizer->fetchData($prevSyncStatus);

        if (false === $newSyncStatus->equals($prevSyncStatus)) {
            $this->showDescriptionBySyncStatus($io, $newSyncStatus);
        }
    }

    private function showDescriptionBySyncStatus(SymfonyStyle $io, SyncStatus $currentSyncStatus): void
    {
        switch (1) {
            case $currentSyncStatus->equals(SyncStatus::never()):
                $io->comment('Ohh! It is a cold start, so pls wait a moment until we sync a data. It will spend a few seconds.');
                break;
            case $currentSyncStatus->equals(SyncStatus::inProgress()):
                throw new SyncLockException ('Caution! Exactly right now we syncing data, so you should little bit wait for');
                break;
            case $currentSyncStatus->equals(SyncStatus::done()):
                $io->comment('Beauty! All data synced. We are ready to find the best brewery for you!');
                break;
        }
    }

    protected function askWhile(SymfonyStyle $io): void
    {
        do {
            [$chosenField, $filterValue] = $this->filterQuestion($io);

            $this->getResults(new FilterField($chosenField), $filterValue, $io);

            $proceedAnswer = $this->proceedQuestion($io);

        } while ($proceedAnswer === 'yes');
    }

    /**
     * @param SymfonyStyle $io
     *
     * @return string[]
     */
    private function filterQuestion(SymfonyStyle $io): array
    {
        $filters = FilterField::getAvailableValues();
        $question1 = new ChoiceQuestion(
            'Please choose the field of filter (default is\'' . reset($filters) . '\')',
            $filters,
            0
        );
        $question1->setErrorMessage('Field %s is invalid.');
        $chosenField = $io->askQuestion($question1);

        $question2 = new Question('Please enter the value for searching', 'ololo');
        $searchValue = $io->askQuestion($question2);

        $io->text('Your applied filter');
        $io->horizontalTable(['Field Name', 'Search Value'], [[$chosenField, $searchValue]]);

        return [$chosenField, $searchValue];
    }

    private function proceedQuestion(SymfonyStyle $io): string
    {
        $question = new ChoiceQuestion(
            'Will we continue? (default is \'yes\')',
            ['yes', 'no'],
            0
        );

        $answer = $io->askQuestion($question);

        if ($answer === 'no') {
            $io->note('Thanks for your time. Have a nice day.');
        }

        return $answer;
    }

    private function getResults(FilterField $filterField, string $filterValue, SymfonyStyle $io): void
    {
        $results = $this->searchBrewery->search($filterField, $filterValue);

        $io->title('Search results');
        $io->comment('total numbers: ' . count($results));

        foreach ($results as $brewery) {
            $io->horizontalTable(
                ['Name', 'Street', 'City', 'State'],
                [
                    [
                        $brewery->getName(),
                        $brewery->getStreet(),
                        $brewery->getCity(),
                        $brewery->getState(),
                    ],
                ]
            );
        }
    }
}