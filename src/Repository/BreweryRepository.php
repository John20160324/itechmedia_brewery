<?php

namespace App\Repository;

use App\Entity\Brewery;
use App\ValueObject\FilterField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class BreweryRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Brewery::class);
    }

    public function hasData(): bool
    {
        return 0 < $this
                ->createQueryBuilder('b')
                ->select('count(b.id)')
                ->getQuery()
                ->getSingleScalarResult();
    }

    /**
     * @param FilterField $filterField
     * @param string      $value
     *
     * @return Brewery[]
     */
    public function findByFiledAndValue(FilterField $filterField, string $value): array
    {
        $fieldAlias = sprintf('b.%s', $filterField->getValue());
        $condition = '%' . $value . '%';
        $qb = $this->createQueryBuilder('b');

        return $qb
            ->where(
                $qb->expr()->like(
                    $fieldAlias,
                    $qb->expr()->literal($condition)
                )
            )
            ->getQuery()
            ->getResult();
    }
}
