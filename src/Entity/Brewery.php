<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BreweryRepository")
 */
class Brewery
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private ?int $id;

    /**
     * @ORM\Column(name="external_id", type="integer")
     */
    private int $externalId;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private string $name;

    /**
     * @ORM\Column(name="brewery_type", type="string")
     */
    private string $breweryType;

    /**
     * @ORM\Column(name="street", type="string")
     */
    private string $street;

    /**
     * @ORM\Column(name="city", type="string")
     */
    private string $city;

    /**
     * @ORM\Column(name="state", type="string")
     */
    private string $state;

    /**
     * @ORM\Column(name="postal_code", type="string")
     */
    private string $postalCode;

    /**
     * @ORM\Column(name="country", type="string")
     */
    private string $country;

    /**
     * @ORM\Column(name="longitude", type="float", length=10, scale=6)
     */
    private float $longitude;

    /**
     * @ORM\Column(name="latitude", type="float", length=10, scale=6)
     */
    private float $latitude;

    /**
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    private ?string $phone;

    /**
     * @ORM\Column(name="website_url", type="string", nullable=true)
     */
    private ?string $websiteUrl;

    /**
     * @ORM\Column(name="updated_at", type="datetime_immutable")
     */
    private \DateTimeImmutable $updatedAt;

    /**
     * @var string[]
     * @ORM\Column(name="tag_list", type="array")
     */
    private array $tagList = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternalId(): int
    {
        return $this->externalId;
    }

    public function setExternalId(int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBreweryType(): string
    {
        return $this->breweryType;
    }

    public function setBreweryType(string $breweryType): self
    {
        $this->breweryType = $breweryType;

        return $this;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWebsiteUrl(): ?string
    {
        return $this->websiteUrl;
    }

    public function setWebsiteUrl(?string $websiteUrl): self
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTagList(): array
    {
        return $this->tagList;
    }

    public function setTagList(array $tagList): self
    {
        $this->tagList = $tagList;

        return $this;
    }
}
